using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class TowerController : MonoBehaviour
{
    
   
    public float rotationSpeed = 5f;
    public float reloadSpeed = 1f;
    public float range = 15;
    public int TowerLevel = 1;
    public List<Tower> Towers = new List<Tower>();

    private void Start()
    {
        InitTowers();
    }

    public void UpgradeTower(int type)
    {
        switch (type)
        {
            case 0:
                reloadSpeed += 0.3f;
                break;
            case 1:
                range += 2;
                break;
            case 2:
                TowerLevel++;
                break;
            default:
                break;
        }
        InitTowers();
    }
    
    public void InitTowers()
    {
        for (var index = 0; index < TowerLevel; index++)
        {
            var tower = Towers[index];
            tower.gameObject.SetActive(true);
            tower.reloadSpeed = reloadSpeed;
            tower.range = range + TowerLevel;
            tower.rotationSpeed = rotationSpeed;
        }
    }

}

