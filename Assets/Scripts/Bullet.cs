using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Ammo
{
 

    private Vector3 startPos;           
    private Vector3 targetPos;          
    private float distance;             
    private float startTime;            
    private bool isCreate;
    
    public override void CreateAmmo(Transform target)
    {
        startTime = Time.time;
        startPos = transform.position;
        targetPos = target.position;
        distance = Vector3.Distance(startPos, targetPos);
        isCreate = true;
    }

    void Update()
    {
        if(!isCreate) return;
        
        
        float timeSinceStart = Time.time - startTime;
        float fractionOfJourney = timeSinceStart * speed / distance;
        transform.position = Vector3.Lerp(startPos, targetPos, fractionOfJourney);

       
        if (fractionOfJourney >= 1 ||Vector3.Distance(transform.position, startPos) > maxDistance)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag($"Enemy")) return;
        
        IDamagable damagable = collision.gameObject.GetComponent<IDamagable>();
        
        if (damagable != null)
        {
            damagable.TakeDamage(damage);
            Destroy(gameObject);
        }
        
    }
}
