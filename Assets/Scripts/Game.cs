using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using Random = System.Random;

public class Game : MonoBehaviour
{
    public List<Enemy> EnemiesInBattle = new List<Enemy>();
    public List<Transform> EnemySpawnPoints = new List<Transform>();
    public GameObject enemyPrefab;
    public TowerController Player;
    public float spawnTime;
    public int Money;
    public int KillScore;
    public int GameLevel = 1;
    
    public static Game Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void RemoveMoney(int val)
    {
        Money -= val;
        Events.OnUpdateUI();
    }
    
    public void RemoveMoneyTower()
    {
        Money -= (Player.TowerLevel - 1) * 10;
        Events.OnUpdateUI();
    }
    
    private void Start()
    {
        Events.OnCreateEnemyAction += OnCreate;
        Events.OnDeadEnemyAction += OnEnemyDead;
        StartCoroutine(Spawn());
    }

    private Enemy OnCreate(Enemy enemy)
    {
        EnemiesInBattle.Add(enemy);
        return enemy;
    }
    
    private Enemy OnEnemyDead(Enemy enemy)
    {
        EnemiesInBattle.Remove(enemy);
        KillScore++;
        Money++;
        GameLevel = KillScore / 10;
        Events.OnUpdateUI();
        return enemy;
    }

    public Transform GetNearestEnemy()
    {
        Transform nearestEnemy = null;
        float shortestDistance = Mathf.Infinity;
        Vector3 currentPosition = transform.position;

        foreach (Enemy enemy in EnemiesInBattle)
        {
            float distanceToEnemy = Vector3.Distance(currentPosition, enemy.transform.position);

            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy.transform;
            }
        }

        return nearestEnemy;
    }

    private IEnumerator Spawn()
    {
        yield return new WaitForSeconds(GetModTime());
        Transform randomPoint = EnemySpawnPoints[UnityEngine.Random.Range(0, EnemySpawnPoints.Count)];
        Enemy enemy = Instantiate(enemyPrefab, randomPoint.position, Quaternion.identity).GetComponent<Enemy>();
        enemy.Create(Player.transform);
        StartCoroutine(Spawn());
    
    }

    private float GetModTime()
    {
        float modtime = spawnTime - GameLevel / 10;
        modtime = Mathf.Clamp(modtime, 0.5f, 2);
        return modtime;
    }

    private void OnDestroy()
    {
        Events.OnCreateEnemyAction -= OnCreate;
        Events.OnDeadEnemyAction -= OnEnemyDead;
    }
}
