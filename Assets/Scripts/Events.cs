using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Events
{
   public static Func<Enemy, Enemy> OnCreateEnemyAction;
   public static Func<Enemy, Enemy> OnDeadEnemyAction;
   public static Action OnUpadateUiAction;

   public static Enemy OnEnemyCreate(Enemy enemy)
   {
      if (OnCreateEnemyAction != null)
      {
         OnCreateEnemyAction(enemy);
      }
      return enemy;
   }

   public static Enemy OnEnemyDead(Enemy enemy)
   {
      if (OnDeadEnemyAction != null)
      {
         OnDeadEnemyAction(enemy);
      }
      return enemy;
   }

   public static void OnUpdateUI()
   {
      if (OnUpadateUiAction != null)
      {
         OnUpadateUiAction();
      }
   }
}
