using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public TurretRotator Rotator;
    public Ammo bullet;
    public Transform shootPoint;
    public Transform target;
    public float rotationSpeed;
    public float reloadSpeed;
    public float range;
    
    private float lastFireTime;
    
    private void Start()
    {
        Rotator = new TurretRotator();
        StartCoroutine(FindEnemyDelay());
    }

    void Update()
    {
        if(!target) return;
        if(Vector3.Distance(transform.position, target.position)> range) return;
        
        Rotator.Rotate(target, transform, rotationSpeed);
        Reloading();
    }

    private IEnumerator FindEnemyDelay()
    {
        while (true)
        {
            if (target == null)
            {
                target = Game.Instance.GetNearestEnemy();
            }
            yield return new WaitForSeconds(0.2f);
        }
    }
    
    private void Reloading()
    {
        if (Time.time > lastFireTime + 1 / reloadSpeed)
        {
            lastFireTime = Time.time; 
           
            Shoot(target);
        }
    }
    
    private void Shoot(Transform target)
    {
        if(!target) return;
        if(Vector3.Distance(transform.position, target.position)> range) return;
        
        Ammo ammo = Instantiate(bullet, shootPoint.position, Quaternion.identity);
        ammo.CreateAmmo(target);
    }
}

public class TurretRotator
{
    public void Rotate(Transform target, Transform self, float speed)
    {
        if (target != null)
        {
            // Определяем направление до цели
            Vector3 direction = target.position - self.position;
            direction.y = 0;

            // Поворачиваем башню в направлении цели
            Quaternion targetRotation = Quaternion.LookRotation(direction);
            self.rotation = Quaternion.Slerp(self.rotation, targetRotation, speed * Time.deltaTime);
        }
    }
}
