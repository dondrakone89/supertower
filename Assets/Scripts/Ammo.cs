using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    public float speed = 25f;           // скорость пули
    public float maxDistance = 100f;    // максимальная дистанция полета
    public int damage;
    public Vector3 target;           // цель для попадания

    public virtual void CreateAmmo(Transform target)
    {
        
    }
}
