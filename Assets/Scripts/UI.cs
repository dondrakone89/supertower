using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
   public TMP_Text levelText;
   public TMP_Text ScoreText;
   public TMP_Text MoneyText;
   public TMP_Text RPMText;
   public TMP_Text RangeText;
   public TMP_Text TowerCostText;
   public Button TowerButton;
   public List<Button> UpgradeButtons = new List<Button>();
   
   private Game _game;
   private void Start()
   {
      Events.OnUpadateUiAction += UpDateUI;
      _game = Game.Instance;
      UpDateUI();
   }

   private void UpDateUI()
   {
      levelText.text = _game.GameLevel.ToString();
      ScoreText.text = _game.KillScore.ToString();
      MoneyText.text = _game.Money.ToString();
      RPMText.text = _game.Player.reloadSpeed.ToString("F");
      RangeText.text = _game.Player.range.ToString("0000");
      TowerCostText.text = (_game.Player.TowerLevel * 10f).ToString();

      TowerButton.interactable = _game.Money >= _game.Player.TowerLevel * 10f && _game.Player.TowerLevel < 4;
      
      foreach (var button in UpgradeButtons)
      {
         button.interactable = _game.Money >= 10;
      }

   }

   private void OnDestroy()
   {
      Events.OnUpadateUiAction -= UpDateUI;
   }
}
