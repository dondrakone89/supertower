using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = System.Random;

public class Enemy : MonoBehaviour, IDamagable
{
    public int hitPoint;
    public float speed;
    public Transform player;
    
    private EnemyMover Mover;

    public void Create(Transform _player)
    {
        player = _player;
        Mover = new EnemyMover(player, this);
        hitPoint = UnityEngine.Random.Range(1, 4);
        transform.localScale += Vector3.one * hitPoint;
        Events.OnEnemyCreate(this);
    }

    private void Update()
    {
        Mover.Move(speed);
    }

    public void TakeDamage(int value)
    {
        print("Hit! " + value);
        hitPoint -= value;
        
        if (hitPoint <= 0)
        {
            Events.OnEnemyDead(this);
            Destroy(gameObject);
        }
    }
}

public class EnemyMover
{
    private Transform Target;
    private readonly Enemy Self;

    
    private Vector3 startPos;           
    private Vector3 targetPos;          
    private float distance;             
    private float startTime;     
    
    
    public EnemyMover(Transform target, Enemy self)
    {
        Target = target;
        Self = self;
        
        startTime = Time.time;
        startPos = self.transform.position;
        targetPos = target.position;
        distance = Vector3.Distance(startPos, targetPos);
    }
    
    public void Move(float speed)
    {
        float timeSinceStart = Time.time - startTime;
        float fractionOfJourney = timeSinceStart * speed / distance;
        Self.transform.position = Vector3.Lerp(startPos, targetPos, fractionOfJourney);

        if (fractionOfJourney >= 0.9f)
        {
            Events.OnEnemyDead(Self);
            Debug.Log("Kill");
            SceneManager.LoadScene(0);
            UnityEngine.GameObject.Destroy(Self.gameObject);
        }
    }
}
